<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/cajero','Cajero\CajeroController')->only(['index']);
Route::post('/deposito','Cajero\CajeroController@procesaDeposito')->name('deposito');
Route::post('/retiro','Cajero\CajeroController@procesaRetiro')->name('retiro');

Route::get('/depositar','General\GeneralController@depositar')->name('frmDepositar');
Route::get('/retitar','General\GeneralController@retirar')->name('frmRetitar');
Route::get('/account','General\GeneralController@showAccount')->name('cuenta');
