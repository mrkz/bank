<?php

namespace App\Model\Account;

use Illuminate\Database\Eloquent\Model;
use App\User; 

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'type','account','balance'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
