<?php

namespace App\Http\Controllers\Cajero;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Account\Account;


class CajeroController extends Controller
{
    const ACCOUNT = 'account';

    public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cashmachine.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function procesaDeposito(Request $request)
    {
        $this->validateRequest($request);

        $account = $this->getAccount();
        $account->balance += $request->monto;
        $account->update();
        $this->setSessionAccount($account);
        return back()->with('msgSuccess','Deposito realizado con exito!');
    }
    
    public function procesaRetiro(Request $request)
    {
        $this->validateRequest($request);

        $account            = $this->getAccount();
        $comision           = ($account->type == 'CREDITO' ? $this->getTotalComision($request->monto) : 0);

        $total      = ($comision + $request->monto);
        $validate   = $this->validateAmount($total);

        if(!$validate) {
            
            $account->balance   -= $total;
            $account->update();
            
            $this->setSessionAccount($account);
            
            return back()->with('msgSuccess','Retiro realizado con exito!');
        }
        else {
            return back()->with('msgError','Monto a retirar no valido!'); 
        }
    }
    

    private function validateRequest($request)
    {
        $request->validate([
            'monto'=>'required|min:1|numeric'
        ]);
    }

    private function getTotalComision($monto)
    {
        return (0.10 * $monto);        
    }

    private function setSessionAccount($account)
    {
        session(['account'=>$account]);
    }

    private function validateAmount($retiro)
    {   
        $account = $this->getAccount();
        if($retiro == 0  || $retiro > $account->balance) {
            return true;
        }
        return false;
    }
    
    private function getAccount()
    {
        return Account::where(self::ACCOUNT,session(self::ACCOUNT)->account)->first();
    }

}
