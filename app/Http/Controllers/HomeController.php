<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Account\Account;
use App\User;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $accounts   = User::find(Auth::id());
        $info       = $accounts->accounts->toArray();

        return view('home',['accounts'=>$info]);
    }
}
