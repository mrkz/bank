<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Account\Account;

class GeneralController extends Controller
{
    public function depositar()
    {
        return view('cashmachine.depositar');
    }

     public function retirar()
    {
    	return view('cashmachine.retirar');
    }

    public function showAccount(Request $request)
    {   
        $account = $this->getDataAccount($request->nip);
        session(['account'=>$account]);
        return view('account.index',['account'=>$account]);
    }

    public function getDataAccount($nip)
    {
        return Account::where('nip',$nip)->first();
    }
}
