function updateSanbox(){

	var trade_id = $('#trade_id').val();
	var name_sandbox = $('#nombre_sanbox').val();
	var sandbox_id = $('#id_sanbox').val();
	
	if(trade_id != "" && name_sandbox != "" && sandbox_id) {
		$.ajax({
			url: '/shops/'+trade_id,
			type: 'PATCH',
			data: {
				trade_id: trade_id,
				sandbox_name: name_sandbox,
				sandbox_id: sandbox_id,
				_token: $("input[name='_token']").val()
			},
			success: function(response){
				if(response.success === true) {
					alert('Parametros de sandbox actualizados correctamente.');
				}
			}
		});
	}
	else {
		alert('Nombre y el valor Id en sandbox son requeridos');
		return false;
	}
}


$(document).ready(function () {
  // Default Datatable
	  $('#list_document, #list_users').DataTable({
	    "language": {
	      "paginate": {
	        "previous": "<i class='mdi mdi-chevron-left'>",
	        "next": "<i class='mdi mdi-chevron-right'>"
	      }
	    },
	    "lengthChange": false,
		"searching": true,
	    "drawCallback": function drawCallback() {
	      $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
	    }
	  }); //

  });