<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'Marcos Martinez Hernandez',
            'email'     => 'mark.mz.hz@gmail.com',
            'password'  => Hash::make('marko5')
        ]);

        DB::table('accounts')->insert([
            'user_id'   => 1,
            'type'      => 'DEBITO',
            'account'   => '123456789',
            'balance'   => 1000,
            'nip'       => hash('sha256','1234')
        ]);

        DB::table('accounts')->insert([
            'user_id'   => 1,
            'type'      => 'CREDITO',
            'account'   => '987654321',
            'balance'   => 1000,
            'nip'       => hash('sha256','4321')
        ]);
    }
}