## Cajero Automatico

Aplicacion cajero automatico

## Requerimientos

- Administrador de dependencias: composer
- PHP  > 7.2.5
- Manejador de base de datos mysql > 5.6
- Copiar .env.example a .env en el directorio raiz del proyecto

## Instalación

- ** 1.- Crear una base de datos de nombre cajero **
- ** 2.- Ejecutar la sentencia "composer install" en el directorio del proyecto **
- ** 3.- Ejecutar la siguiente sentencia "php artisan migrate" para generar las tablas de la base de datos. **
- ** 4.- Agregar usuario de prueba con la siguiente sentencia "php artisan migrate:refresh --seed" **

## Nota

Para entrar al cms cajero:

	usuario: mark.mz.hz@gmail.com
	password: marko5																	


## Autor

	Marcos Martinez Hernandez
	mark.mz.hz@gmail.com
	55 13 71 11 62