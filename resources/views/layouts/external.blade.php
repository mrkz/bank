<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        @include('layouts.head')
    </head>
    <body>
        <!-- Begin page -->
        <div id="wrapper" style="padding: 0px;">
            @include('layouts.externaltopbar')
            <div class="container-fluid">
                <!-- Start content -->
                <div class="content">
                    @yield('content')
                </div> <!-- content -->
                @include('layouts.footer')    
            </div>
        </div>
        <!-- END wrapper -->
        @include('layouts.footer-script')    
    </body>
</html>