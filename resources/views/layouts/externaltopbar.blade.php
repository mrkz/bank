<!-- Topbar Start -->
<div class="navbar-custom">
    <!-- LOGO -->
    <div class="logo-box">
        <a href="#" class="logo text-center">
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-sm.png') }}" alt="Banco MX" height="19">
                <!-- <span class="logo-lg-text-light">UBold</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">U</span> -->
                <img src="{{ URL::asset('assets/images/logo-sm.png') }}" alt="Banco MX" height="19">
            </span>
        </a>
    </div>
</div>
<!-- end Topbar -->