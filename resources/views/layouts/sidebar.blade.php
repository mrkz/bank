<!-- ========== Left Sidebar Start  -->
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Modulos</li>
                @if(Auth::user()->type == 1)
                <li>
                    <a href="{{ route('users.index') }}">
                        <i class="fe-users"></i>
                        <span> Users</span>
                    </a>
                </li>
                @endif
               @if(Auth::user()->type == 0)
                <li>
                    <a href="/">
                        <i class="fe-archive"></i>
                        <span> Documents</span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->