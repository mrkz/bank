@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
    @foreach($accounts as $account)
                    <div class="col-lg-6 col-xs-12">
                        <div class="card card-body">
                            <h4 class="card-title">SALDO ACTUAL $ {{ $account['balance'] }} mxn</h4>
                            <p class="card-text">Realice operaciones en su cuenta</p>
                            <a href="{{ route('cuenta') }}?nip={{ $account['nip'] }}" class="btn btn-primary waves-effect waves-light">CUENTA DE {{ $account['type'] }}</a>
                        </div>
                    </div>                
    @endforeach
    </div>
</div>
@endsection
