@extends('layouts.master')
@section('content')
<div class="container">
    @include('widget.widget')
    <div class="row justify-content-center">
        <div class="col-lg-6 col-xs-12">
            <div class="card card-body">
                <h3 class="card-title text-center">Retirar efectivo</h3>
                @if($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if(session()->has('msgSuccess'))
                <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                    <p>{{ session('msgSuccess') }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                 @if(session()->has('msgError'))
                <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                    <p>{{ session('msgError') }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form class="form-horizontal" action="{{ route('retiro') }}" method="POST">
                    @csrf
                    <div class="form-group row mb-3">
                        <label for="monto" class="col-4 col-form-label">Monto</label>
                        <div class="col-8">
                            <input type="text" class="form-control" name="monto" id="monto" placeholder="Introduzca la cantidad" maxlength="10" required="required">
                        </div>
                    </div>
                    <div class="form-group mb-0 justify-content-end row">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">RETIRAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
