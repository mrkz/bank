<div class="row justify-content-center">
    <div class="col-lg-6 col-xs-12">
        <div class="card card-body">
            <h5 class="card-title">Depositar Efectivo</h5>
            <p class="card-text">Realice deposito de efectivo a su cuenta</p>
            <a href="{{ route('frmDepositar') }}" class="btn btn-primary waves-effect waves-light">DEPOSITAR</a>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="card card-body">
            <h5 class="card-title">Retiro de efectivo</h5>
            <p class="card-text">Realice operaciones en su cuenta</p>
            <a href="{{ route('frmRetitar') }}" class="btn btn-primary waves-effect waves-light">RETIRAR EFECTIVO</a>
        </div>
    </div>
</div>