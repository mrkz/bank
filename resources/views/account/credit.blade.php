<div class="row justify-content-center">
    <div class="col-lg-6 col-xs-12">
        <div class="card card-body">
            <h5 class="card-title">Pagar Tarjeta</h5>
            <p class="card-text">Realice pagos en efectivo a su cuenta de credito</p>
            <a href="{{ route('frmDepositar') }}" class="btn btn-primary waves-effect waves-light">PAGAR</a>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="card card-body">
            <h5 class="card-title">Retiro de efectivo</h5>
            <p class="card-text">Realice operaciones en su cuenta de crédito ó débito</p>
            <a href="{{ route('frmRetitar') }}" class="btn btn-primary waves-effect waves-light">RETIRAR EFECTIVO</a>
        </div>
    </div>
</div>