@extends('layouts.master')
@section('content')
<div class="container">
    @include('widget.widget')
    @if($account->type == 'DEBITO')
        @include('account.debit')
    @else
        @include('account.credit')
    @endif
</div>
@endsection