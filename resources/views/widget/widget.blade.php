<div class="row justify-content-center">
    <div class="col-lg-12 col-xs-12">
        <div class="card card-body text-center">
            <h2 class="card-title">Banco MX</h2>
            <p class="card-text" style="font-size: 19px;">Seleccione una operación</p>
            <div class="row">
                <div class="col-lg-5">
                    <p class="text-left" style="font-size: 19px; font-weight: bold;">Saldo Actual: ${{ session('account')->balance }} mxn</p>
                </div>
                <div class="col-lg-2">
                    <a href="{{ route('home') }}">Ir a inicio</a>
                </div>
                <div class="col-lg-5">
                    <p class="text-right" style="font-size: 19px; font-weight: bold;">Cuenta: {{ session('account')->type }}</p>
                </div>
            </div>
        </div>
    </div>
</div>